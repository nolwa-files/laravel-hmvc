User module
1.user
2.roles and permissions
User 
User CRUD
Change password
Option to send password on email[will update after email template functional]
Roles and permissions using spatie/laravel-permission
Role CRUD

There are 4 restrictions
List
Create
Edit
Delete

The functions can be restricted in the view part
Say for example
 @can('role-list')
……………………………
@endcan 

@can('role-crete')
……………………………
@endcan

@can('role-edit')
……………………………
@endcan

@can('role-delete’')
……………………………
@endcan


Also in controller set constructor like this
	function __construct()

	{

   	$this->middleware('permission:role-list');

   	$this->middleware('permission:role-create', ['only' => ['create','store']]);

   	$this->middleware('permission:role-edit', ['only' => ['edit','update']]);

   	$this->middleware('permission:role-delete', ['only' => ['destroy']]);

   }


Currency configuration using Torann\Currency
Email Configuration for smtp protocol
